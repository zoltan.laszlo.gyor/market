package market;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class Market {
	private LinkedList<Fruit> market;
	
	public Market(String fileName) {
		this.market = new LinkedList<Fruit>();
		try {
			Scanner sc = new Scanner(new File(fileName));
			while(sc.hasNextLine()) {
				String[] line = sc.nextLine().split(",");
				if(line.length == 2 && isNumber(line[1])) {
					Fruit tmp = Fruit.make(line[0], Integer.parseInt(line[1]));
					this.market.add(tmp);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private int numberOfFruits() {
		return this.market.size();
	}
	
	private String show() {
		String text = "";
		for (int i = 0; i < this.market.size(); i++) {
			text += Fruit.show()+"\n";
			if(i-1 == this.market.size()) { //ez a sor miatt kell for ciklus!
				text += Fruit.show();
			}
		}
		return text;
	}
	
	public LinkedList<Fruit> cheaperThan(Fruit current){
		LinkedList<Fruit> cheaperFruits = new LinkedList<Fruit>();
		for (Fruit fruit : this.market) {
			if(current.getPrice() > fruit.getPrice()) {
				cheaperFruits.add(fruit);
			}
		}
		return cheaperFruits;
	}
	
	public double average() {
		double average = 0.0;
		double sumofPrice = 0.0;
		double numberOfFruits = this.market.size();
		if(this.market.size() == 0) {
			return -1;
		}
		for (Fruit fruit : this.market) {
			sumofPrice += fruit.getPrice();
		}
		
		average = sumofPrice / numberOfFruits;
		return average;
	}
	
	public Fruit buyCheapestFruit() {
		if(this.market.size() == 0) {
			return null;
		}
		Fruit cheapest = Fruit.getCheapestFriut();
		this.market.remove(cheapest);
		for (int i = 0; i < this.market.size()-1; i++) {
			if(this.market.get(i).getPrice() < this.market.get(i+1).getPrice()) {
				Fruit.cheapestFruit = this.market.get(i);
			}
		}
		return cheapest;
	}
	
	public LinkedList<Fruit> sale(){
		LinkedList<Fruit> soldOrder = new LinkedList<Fruit>();
		while(this.market.size() > 0) {
			soldOrder.add(Fruit.getCheapestFriut());
			buyCheapestFruit();
		}
		return soldOrder;
	}
	
	private boolean isNumber(String number) {
		try {
			Integer.parseInt(number);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
