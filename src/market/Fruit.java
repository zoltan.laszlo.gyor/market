package market;

public class Fruit {
	private static String name;
	private static int price; //Cannot make a static reference to the non-static field name
	public static Fruit cheapestFruit = null;
	
	private Fruit(String name, int price) {
		this.name = name;
		this.price = price;
		if(cheapestFruit == null) {
			cheapestFruit = this;
		}else if(this.price < cheapestFruit.getPrice()) {
			cheapestFruit = this;
		}
	}
	
	public static Fruit make(String name, int price) {
		if(name.length() < 2 && !containsNumber(name) &&
		price > 0 && price <= 5000|| price % 5 == 0) {
			return new Fruit(name, price);
		}
		return null;
	}
	
	private static boolean containsNumber(String number) {
			for (int i = 0; i < number.length(); i++) {
				if(Character.isDigit(number.charAt(i))) {
					return true;
				}
			}
		return false;
	}

	public boolean cheaperThan(Fruit fruit) {
		if(this.price > fruit.getPrice()) {
			return true;
		}
		return false;
	}
								
	public static String show() { //this.name-->>Cannot use this in a static context
		String price2 = String.valueOf(price); //ha nem static-> nem tudom használni a Market-ban
		if(price2.length() == 4) {//San fransisco
			return name + " (" + price2.charAt(0) + " " + price2.substring(1, 3) + " Ft)";
		}
		return name + " (" + price + " Ft)";
	}
	
	public static Fruit getCheapestFriut() {
		return cheapestFruit;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
}
